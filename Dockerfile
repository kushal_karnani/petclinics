FROM openjdk:8
COPY target/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar .
EXPOSE 8080
ENTRYPOINT ["java","-jar","spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar"]

